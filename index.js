const express = require ('express');
// create an application using express
// this creates an express application and stores this as a constant called app. 
//app is our server
const app = express()
//for our application server to run, we need a port to listen to.
const port = 3000;

//Setup for allowing the server to handle data from requests
//Allows your app to read json data
//Methods used from express JS are middlewares
//Middleware is a software that provides services outside of what's offered by the operating system 
app.use(express.json());
//allows your app to road data from forms
//Be default, information received from the url can only be received as a string or an array
//By applying the option of "extended:true" this allows us to receive information in other data types such as an object/boolean etc, which we will use throught our application
app.use(express.urlencoded({ extended: true}))

//Database connection
const mongoose = require('mongoose');
mongoose.connection.once('open', () =>{
	console.log("Now connected to s30 database")
});

mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.o2ycp.mongodb.net/s30?retryWrites=true&w=majority',{
	    useNewUrlParser: true,
	    useUnifiedTopology: true
})

//User Model
const userSchema = new mongoose.Schema({   

	firstName: {
		type: String
	},
	lastName: {
		type: String
	},
	email: 	{
		type:String,
	},
	password: {
		type: String,
	},
	isAdmin: {
		type: Boolean
	}

})

const User = mongoose.model("User", userSchema)


//Routes and function

app.post('/signup', (req,res) =>{
	User.findOne({email:req.body.email}, (err,result) =>{
		if(result !== null && result.email == req.body.email){
			return res.send("User already registered")
		}else{
			let newUser = new User({	
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				password: req.body.password,
				isAdmin: req.body.isAdmin
			})
			newUser.save((saveErr, savedUser) =>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("User successfully registered")
				}
			})
		}
	})
})

app.get('/users', (req,res) =>{
	User.find({}, (err,result) =>{
		
		if(err){
			return cosole.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})




//update the password of a user that matches the information provided in the client /postman
app.listen(port, () => console.log(`Server is running at port ${port}`))